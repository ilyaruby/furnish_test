# @encoding: UTF-8
require 'spec_helper'

describe "Text parser" do

  describe :merge_tags do
    it "should match single %{asdfasdf}" do
      text = "Вам начислено %{CREDIT:ACHIEVED} баллов. Вам не хватает %{CREDIT:LEVELUP} баллов до получения %{BADGE:LEVELUP}.\n\n%{Samplebox:APP:[$caption=Нажмите здесь для получения скидки]}"
      expected_result = "Вам начислено 1234 баллов. Вам не хватает 15 баллов до получения Silver Badge.\n\n[Нажмите здесь для получения скидки](https://samplebox.test)"
      User.new.merge_tags(text).should == expected_result
    end
  end

end
