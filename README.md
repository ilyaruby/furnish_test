# Интерполятор

Наш маркетинг хочет отправлять почту нашим пользователям.

Письмо должно содержать макро-переменные,
заполняемые в зависимости от текущего пользователя.

Ваша цель написать движок интерполяции, называемый merge-tags.

Предоставьте Ваше решение как pull request на github.

    class Credits
      def achieved
        1234
      end

      def levelup
         15
      end
    end

    class Badge
      def self.levelup
        "Silver Badge"
      end
    end

    class Samplebox::App
      attr_accessor :caption
      def initialize(params)
        @caption = params[:caption]
      end

      def to_s
        "[#{caption}](https://samplebox.test/)"
      end
    end

    class User
      attr_accessor :credits, :facebook, :badge
      def initialize
        @credits  = Credits.new
        @facebook = Facebook.new
        @badge    = Badge.new
      end

      def merge_tags(text)
      end
    end

    # text_parser_spec.rb
    ...
    describe "merge_tags" do
      it "should interpolate" do
        text = "Вам начислено %{CREDIT:ACHIEVED} баллов. Вам не хватает %{CREDIT:LEVELUP} баллов до получения %{BADGE:LEVELUP}.\n\n%{Samplebox:APP:[$caption=Нажмите здесь для получения скидки]}"
        expected_result = "Вам начислено 1234 баллов. Вам не хватает 15 баллов до получения Silver Badge.\n\n[Нажмите здесь для получения скидки](https://samplebox.test)"
        User.new.merge_tags(text).should == expected_result
      end
    end
