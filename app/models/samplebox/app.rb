class Samplebox::App
  attr_accessor :caption
  def initialize(params)
    @caption = params[:caption]
  end

  def to_s
    "[#{caption}](https://samplebox.test)"
  end
end
